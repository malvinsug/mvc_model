import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class View extends JFrame implements PropertyChangeListener {
  private static final int MINIMUM_FRAME_HEIGHT = 200;
  private static final int MINIMUM_FRAME_WIDTH = 400;

  private static final String UNIT_CONVERTER = "Unit Converter";

  private final Controller controller;
  private final Model model;

  private JLabel label;
  private JSlider kiloSlider;
  private JSlider mileSlider;
  private boolean isSliderActive = true;

  ChangeListener kiloSliderListener;
  ChangeListener milesSliderListener;

  View(Controller controller,Model model) {
    super(View.UNIT_CONVERTER);
    this.controller = controller;
    this.model = model;
    model.addPropertyChangeListener(this);

    setMinimumSize(new Dimension(MINIMUM_FRAME_WIDTH,MINIMUM_FRAME_HEIGHT));
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    createWidgets();
    setVisible(true);
  }

  public void setSliderActive(boolean isSliderActive) {
    this.isSliderActive = isSliderActive;
  }

  private void createWidgets() {
    setLayout(new GridBagLayout());

    JPanel panel = new JPanel();
    panel.setPreferredSize(new Dimension(MINIMUM_FRAME_WIDTH, MINIMUM_FRAME_HEIGHT));

    GridBagConstraints c = new GridBagConstraints();
    c.anchor = GridBagConstraints.CENTER;
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0f;
    c.weighty = 1.0f;
    add(panel, c);

    panel.setLayout(new GridBagLayout());
    c = new GridBagConstraints();
    c.gridx = 0;
    c.insets = new Insets(0, 10, 0, 10);

    label = new JLabel("0 kilometers equals 0 miles");
    panel.add(label, c);

    kiloSlider = createKiloSlider();
    JLabel kilometer = new JLabel("Kilometer");
    JPanel kiloPanel = createSliderWidget(kiloSlider, kilometer);
    panel.add(kiloPanel, c);

    mileSlider = createMileSlider();
    JLabel miles = new JLabel("Miles");
    JPanel milePanel = createSliderWidget(mileSlider, miles);
    panel.add(milePanel,c);
  }

  private JPanel createSliderWidget(JSlider kiloSlider, JLabel kilometer) {
    JPanel kiloPanel = new JPanel();
    kiloPanel.setLayout(new FlowLayout());
    kiloPanel.add(kiloSlider);
    kiloPanel.add(kilometer);
    return kiloPanel;
  }

  private JSlider createKiloSlider() {
    JSlider slider = new JSlider(JSlider.HORIZONTAL,0,160,0);
    slider.setMajorTickSpacing(40);
    slider.setMinorTickSpacing(10);
    slider.setPaintTicks(true);
    slider.setPaintLabels(true);
    kiloSliderListener = new SliderListener("KILO");
    slider.addChangeListener(kiloSliderListener);
    return slider;
  }

  private JSlider createMileSlider() {
    JSlider slider = new JSlider(JSlider.HORIZONTAL,0,100,0);
    slider.setMajorTickSpacing(20);
    slider.setMinorTickSpacing(10);
    slider.setPaintTicks(true);
    slider.setPaintLabels(true);
    milesSliderListener = new SliderListener("MILES");
    slider.addChangeListener(milesSliderListener);
    return slider;
  }

  private void updateKiloMilesText() {
    int kilo = model.getKiloMiles();
    int miles = model.getMilesKilo();
    label.setText(kilo + " kilometers equals to "+ miles + " miles");
  }

  private void updateMilesKiloText() {
    int kilo = model.getKiloMiles();
    int miles = model.getMilesKilo();
    label.setText(kilo + " kilometers equals to "+ miles + " miles");
  }

  @Override
  public void propertyChange(PropertyChangeEvent event) {
    SwingUtilities.invokeLater(
            new Runnable() {
              @Override
              public void run() {
                handleChangeEvent(event);
              }
            }
    );
  }

  private void handleChangeEvent(PropertyChangeEvent event) {
    if (event.getPropertyName().equals(Model.KILO_CHANGED)) {
      int miles = controller.updateMilesSlider();
      System.out.println(miles);
      setSliderActive(false);
      mileSlider.setValue(miles);
      updateKiloMilesText();
      setSliderActive(true);
    } else if (event.getPropertyName().equals(Model.MILE_CHANGED)){
      int kilo = controller.updateKiloSlider();
      setSliderActive(false);
      kiloSlider.setValue(kilo);
      updateMilesKiloText();
      setSliderActive(true);
    }

  }

  public class SliderListener implements ChangeListener {

    private String kiloOrMiles;

    SliderListener(String kiloOrMiles) {
      this.kiloOrMiles = kiloOrMiles;
    }

    @Override
    public void stateChanged(ChangeEvent event) {
      JSlider source = (JSlider) event.getSource();
      if (!source.getValueIsAdjusting() && isSliderActive) {
        int value = source.getValue();
        if (kiloOrMiles == "KILO") {
          controller.setKilo(value);
        } else if (kiloOrMiles == "MILES") {
          controller.setMiles(value);
        }
      }
    }
  }
}
