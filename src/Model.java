import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;


import static java.util.Objects.requireNonNull;

public class Model {
  private int kilo;
  private int miles;

  private static final double KILO_TO_MILE_CONSTANTS = 0.621371;
  public static final String KILO_CHANGED = "KILO CHANGED";
  public static final String MILE_CHANGED = "MILE CHANGED";

  private final PropertyChangeSupport support = new PropertyChangeSupport(this);


  public Model() {
    kilo = 0;
    miles = 0;
  }

  public int getKiloMiles() {
    return this.kilo;
  }

  public int getMilesKilo() {
    return this.miles;
  }

  public void setKiloMiles(int kilo) {
    this.kilo = kilo;
    this.miles = (int) (kilo*KILO_TO_MILE_CONSTANTS);
    notifyListeners(KILO_CHANGED);
  }

  public void setMilesKilo(int miles) {
    this.miles = miles;
    this.kilo = (int) (miles/KILO_TO_MILE_CONSTANTS);
    notifyListeners(MILE_CHANGED);
  }

  private void notifyListeners(String change) {
    support.firePropertyChange(change, null, this);
  }

  public void addPropertyChangeListener(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    support.addPropertyChangeListener(pcl);
  }

}
