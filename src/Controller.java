import javax.swing.*;

public class Controller {
  private Model model;
  private View view;


  public void setView(View view) {
    this.view = view;
  }

  public void setModel(Model model) {
    this.model = model;
  }

  public int updateKiloSlider() {
    int kilo = model.getKiloMiles();
    return kilo;
  }

  public int updateMilesSlider() {
    int miles = model.getMilesKilo();
    return miles;
  }

  public void setKilo(int kilo) {
    model.setKiloMiles(kilo);
  }

  public void setMiles(int miles) {
    model.setMilesKilo(miles);
  }

}
